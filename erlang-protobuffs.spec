%global realname protobuffs
%global upstream basho
%global enable_test 0
Name:		erlang-%{realname}
Version:	0.9.2
Release:	3
BuildArch:	noarch
Summary:	A set of Protocol Buffers tools and modules for Erlang applications
License:	ASL 2.0
URL:		https://github.com/%{upstream}/erlang_%{realname}
VCS:		scm:git:https://github.com/%{upstream}/erlang_%{realname}.git
Source0:	https://github.com/%{upstream}/erlang_%{realname}/archive/%{version}/%{realname}-%{version}.tar.gz
Source1:	erlang-protobuffs-protoc-erl
BuildRequires:  erlang-meck erlang-rebar erlang-eunit
%description
A set of Protocol Buffers tools and modules for Erlang applications.

%prep
%autosetup -p1 -n erlang_%{realname}-%{version}

%build
%{erlang_compile}

%install
%{erlang_install}
install -D -p -m 0755 %{SOURCE1} %{buildroot}%{_bindir}/protoc-erl

%check
%if %{?enable_test}
%{erlang_test}
%endif

%files
%license LICENSE
%doc AUTHORS CONTRIBUTING.md README.md README_ORIG.md RELNOTES.md
%{_bindir}/protoc-erl
%{erlang_appdir}/

%changelog
* Tue Sep 19 2023 wangkai <13474090681@163.com> - 0.9.2-3
- Disable test for erlang25

* Sun Jul 23 2023 wulei <wu_lei@hoperun.com> - 0.9.2-2
- Fix build error

* Thu Apr 20 2023 wangkai <13474090681@163.com> - 0.9.2-1
- Update to 0.9.2

* Wed Sep 2 2020 yanan li <liyanan032@huawei.com> - 0.9.1-1
- Package init
